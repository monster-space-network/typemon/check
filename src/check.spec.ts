import { Check } from './check';
//
//
//
let count: number = 0;

afterEach(() => {
    count++;
});
afterAll(() => {
    expect(count).toBe(Object.keys(Check).length);
});

test('equal', () => {
    expect(Check.equal(Check, Check)).toBeTruthy();
});
test('not-equal', () => {
    expect(Check.notEqual(Check, Check)).toBeFalsy();
});

test('less', () => {
    expect(Check.less(0, 1)).toBeTruthy();
});
test('less-or-equal', () => {
    expect(Check.lessOrEqual(0, 1)).toBeTruthy();
    expect(Check.lessOrEqual(0, 0)).toBeTruthy();
});

test('greater', () => {
    expect(Check.greater(1, 0)).toBeTruthy();
});
test('greater-or-equal', () => {
    expect(Check.greaterOrEqual(1, 0)).toBeTruthy();
    expect(Check.greaterOrEqual(0, 0)).toBeTruthy();
});

test('is-in', () => {
    expect(Check.isIn('equal', Check)).toBeTruthy();
});
test('is-not-in', () => {
    expect(Check.isNotIn('equal', Check)).toBeFalsy();
});

test('instance-of', () => {
    expect(Check.instanceOf({}, Object)).toBeTruthy();
});
test('not-instance-of', () => {
    expect(Check.notInstanceOf({}, Object)).toBeFalsy();
});

test('is-boolean', () => {
    expect(Check.isBoolean(true)).toBeTruthy();
    expect(Check.isBoolean(false)).toBeTruthy();
});
test('is-not-boolean', () => {
    expect(Check.isNotBoolean(true)).toBeFalsy();
    expect(Check.isNotBoolean(false)).toBeFalsy();
});

test('is-true', () => {
    expect(Check.isTrue(true)).toBeTruthy();
});
test('is-not-true', () => {
    expect(Check.isNotTrue(true)).toBeFalsy();
});

test('is-false', () => {
    expect(Check.isFalse(false)).toBeTruthy();
});
test('is-not-false', () => {
    expect(Check.isNotFalse(false)).toBeFalsy();
});

test('is-truthy', () => {
    expect(Check.isTruthy(true)).toBeTruthy();
    expect(Check.isTruthy(1)).toBeTruthy();
    expect(Check.isTruthy(Infinity)).toBeTruthy();
    expect(Check.isTruthy('check')).toBeTruthy();
});
test('is-falsy', () => {
    expect(Check.isFalsy(undefined)).toBeTruthy();
    expect(Check.isFalsy(null)).toBeTruthy();
    expect(Check.isFalsy(false)).toBeTruthy();
    expect(Check.isFalsy(0)).toBeTruthy();
    expect(Check.isFalsy(NaN)).toBeTruthy();
    expect(Check.isFalsy('')).toBeTruthy();
});

test('is-string', () => {
    expect(Check.isString('check')).toBeTruthy();
});
test('is-not-string', () => {
    expect(Check.isNotString('check')).toBeFalsy();
});

test('is-number', () => {
    expect(Check.isNumber(0)).toBeTruthy();
});
test('is-not-number', () => {
    expect(Check.isNotNumber(0)).toBeFalsy();
});

test('is-nan', () => {
    expect(Check.isNaN(NaN)).toBeTruthy();
});
test('is-not-nan', () => {
    expect(Check.isNotNaN(NaN)).toBeFalsy();
});

test('is-finite', () => {
    expect(Check.isFinite(Infinity)).toBeFalsy();
});
test('is-infinite', () => {
    expect(Check.isInfinite(Infinity)).toBeTruthy();
});

test('is-integer', () => {
    expect(Check.isInteger(1)).toBeTruthy();
});
test('is-not-integer', () => {
    expect(Check.isNotInteger(1)).toBeFalsy();
});

test('is-float', () => {
    expect(Check.isFloat(0.1)).toBeTruthy();
});
test('is-not-float', () => {
    expect(Check.isNotFloat(0.1)).toBeFalsy();
});

test('is-undefined', () => {
    expect(Check.isUndefined(undefined)).toBeTruthy();
});
test('is-not-undefined', () => {
    expect(Check.isNotUndefined(undefined)).toBeFalsy();
});

test('is-null', () => {
    expect(Check.isNull(null)).toBeTruthy();
});
test('is-not-null', () => {
    expect(Check.isNotNull(null)).toBeFalsy();
});

test('is-undefined-or-null', () => {
    expect(Check.isUndefinedOrNull(undefined)).toBeTruthy();
    expect(Check.isUndefinedOrNull(null)).toBeTruthy();
});
test('is-not-undefined-and-not-null', () => {
    expect(Check.isNotUndefinedAndNotNull(undefined)).toBeFalsy();
    expect(Check.isNotUndefinedAndNotNull(null)).toBeFalsy();
});

test('is-symbol', () => {
    expect(Check.isSymbol(Symbol())).toBeTruthy();
});
test('is-not-symbol', () => {
    expect(Check.isNotSymbol(Symbol())).toBeFalsy();
});

test('is-function', () => {
    expect(Check.isFunction(Object)).toBeTruthy();
    expect(Check.isFunction(function () { })).toBeTruthy();
    expect(Check.isFunction(() => { })).toBeTruthy();
});
test('is-not-function', () => {
    expect(Check.isNotFunction(Object)).toBeFalsy();
    expect(Check.isNotFunction(function () { })).toBeFalsy();
    expect(Check.isNotFunction(() => { })).toBeFalsy();
});

test('is-object', () => {
    expect(Check.isObject(null)).toBeFalsy();
    expect(Check.isObject({})).toBeTruthy();
});
test('is-not-object', () => {
    expect(Check.isNotObject(null)).toBeTruthy();
    expect(Check.isNotObject({})).toBeFalsy();
});

test('is-array', () => {
    expect(Check.isArray([])).toBeTruthy();
});
test('is-not-array', () => {
    expect(Check.isNotArray([])).toBeFalsy();
});

test('is-constructor', () => {
    expect(Check.isConstructor({})).toBeFalsy();
    expect(Check.isConstructor(Object)).toBeTruthy();
    expect(Check.isConstructor(function () { })).toBeTruthy();
    expect(Check.isConstructor(() => { })).toBeFalsy();
});
test('is-not-constructor', () => {
    expect(Check.isNotConstructor({})).toBeTruthy();
    expect(Check.isNotConstructor(Object)).toBeFalsy();
    expect(Check.isNotConstructor(function () { })).toBeFalsy();
    expect(Check.isNotConstructor(() => { })).toBeTruthy();
});

test('is-iterable', () => {
    expect(Check.isIterable({ [Symbol.iterator]: () => { } })).toBeTruthy();
    expect(Check.isIterable([])).toBeTruthy();
});
test('is-not-iterable', () => {
    expect(Check.isNotIterable({ [Symbol.iterator]: () => { } })).toBeFalsy();
    expect(Check.isNotIterable([])).toBeFalsy();
});

test('is-async-iterable', () => {
    expect(Check.isAsyncIterable({ [Symbol.asyncIterator]: () => { } })).toBeTruthy();
    expect(Check.isAsyncIterable([])).toBeFalsy();
});
test('is-not-async-iterable', () => {
    expect(Check.isNotAsyncIterable({ [Symbol.asyncIterator]: () => { } })).toBeFalsy();
    expect(Check.isNotAsyncIterable([])).toBeTruthy();
});
