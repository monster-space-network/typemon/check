//
//
//
type FunctionType = (...parameters: any) => any;
type ConstructorType = new (...parameters: any) => any;

export namespace Check {
    /**
     * @example
     * left === right
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Identity
     */
    export function equal(left: unknown, right: unknown): boolean {
        return left === right;
    }
    /**
     * @example
     * left !== right
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Nonidentity
     */
    export function notEqual(left: unknown, right: unknown): boolean {
        return left !== right;
    }

    /**
     * @example
     * left < right
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Less_than_operator
     */
    export function less(left: any, right: any): boolean {
        return left < right;
    }

    /**
     * @example
     * left <= right
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Less_than_or_equal_operator
     */
    export function lessOrEqual(left: any, right: any): boolean {
        return left <= right;
    }

    /**
     * @example
     * left > right
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Greater_than_operator
     */
    export function greater(left: any, right: any): boolean {
        return left > right;
    }

    /**
     * @example
     * left >= right
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Greater_than_or_equal_operator
     */
    export function greaterOrEqual(left: any, right: any): boolean {
        return left >= right;
    }

    /**
     * @example
     * key in target
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/in
     */
    export function isIn<Key extends PropertyKey>(key: Key, target: object): target is Record<Key, unknown> {
        return key in target;
    }

    /**
     * @example
     * !Check.isIn(key, target)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/in
     */
    export function isNotIn<Key extends PropertyKey, Target extends object>(key: Key, target: Target): target is Exclude<Target, Record<Key, unknown>> {
        return !Check.isIn(key, target);
    }

    /**
     * @example
     * value instanceof constructor
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     */
    export function instanceOf<Constructor extends ConstructorType>(value: unknown, constructor: Constructor): value is InstanceType<Constructor> {
        return value instanceof constructor;
    }
    /**
     * @example
     * !Check.instanceOf(value, constructor)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     */
    export function notInstanceOf<Value, Constructor extends ConstructorType>(value: Value, constructor: Constructor): value is Exclude<Value, InstanceType<Constructor>> {
        return !Check.instanceOf(value, constructor);
    }

    /**
     * @example
     * Check.equal(typeof value, 'boolean')
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isBoolean(value: unknown): value is boolean {
        return Check.equal(typeof value, 'boolean');
    }
    /**
     * @example
     * !Check.isBoolean(value)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isNotBoolean<Value>(value: Value): value is Exclude<Value, boolean> {
        return !Check.isBoolean(value);
    }

    /**
     * @example
     * Check.equal(value, true)
     */
    export function isTrue(value: unknown): value is true {
        return Check.equal(value, true);
    }
    /**
     * @example
     * !Check.isTrue(value)
     */
    export function isNotTrue<Value>(value: Value): value is Exclude<Value, true> {
        return !Check.isTrue(value);
    }

    /**
     * @example
     * Check.equal(value, false)
     */
    export function isFalse(value: unknown): value is false {
        return Check.equal(value, false);
    }
    /**
     * @example
     * !Check.isFalse(value)
     */
    export function isNotFalse<Value>(value: Value): value is Exclude<Value, false> {
        return !Check.isFalse(value);
    }

    /**
     * @example
     * !!value
     *
     * @see
     * https://developer.mozilla.org/docs/Glossary/Truthy
     */
    export function isTruthy(value: unknown): boolean {
        return !!value;
    }
    /**
     * @example
     * !value
     *
     * @see
     * https://developer.mozilla.org/docs/Glossary/Falsy
     */
    export function isFalsy(value: unknown): boolean {
        return !value;
    }

    /**
     * @example
     * Check.equal(typeof value, 'string')
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isString(value: unknown): value is string {
        return Check.equal(typeof value, 'string');
    }
    /**
     * @example
     * !Check.isString(value)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isNotString<Value>(value: Value): value is Exclude<Value, string> {
        return !Check.isString(value);
    }

    /**
     * @example
     * Check.equal(typeof value, 'number')
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isNumber(value: unknown): value is number {
        return Check.equal(typeof value, 'number');
    }
    /**
     * @example
     * !Check.isNumber(value)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isNotNumber<Value>(value: Value): value is Exclude<Value, number> {
        return !Check.isNumber(value);
    }

    /**
     * @example
     * Number.isNaN(value)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number/isNaN
     */
    export function isNaN(value: number): boolean {
        return Number.isNaN(value);
    }
    /**
     * @example
     * !Check.isNaN(value)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number/isNaN
     */
    export function isNotNaN(value: number): boolean {
        return !Check.isNaN(value);
    }

    /**
     * @example
     * Number.isFinite(value)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number/isFinite
     */
    export function isFinite(value: number): boolean {
        return Number.isFinite(value);
    }
    /**
     * @example
     * !Check.isFinite(value)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number/isFinite
     */
    export function isInfinite(value: number): boolean {
        return !Check.isFinite(value);
    }

    /**
     * @example
     * Number.isInteger(value)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number/isInteger
     */
    export function isInteger(value: number): boolean {
        return Number.isInteger(value);
    }
    /**
     * @example
     * !Check.isInteger(value)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number/isInteger
     */
    export function isNotInteger(value: number): boolean {
        return !Check.isInteger(value);
    }

    /**
     * @example
     * Check.isFinite(value) && Check.isNotInteger(value)
     */
    export function isFloat(value: number): boolean {
        return Check.isFinite(value) && !Check.isInteger(value);
    }
    /**
     * @example
     * !Check.isFloat(value)
     */
    export function isNotFloat(value: number): boolean {
        return !Check.isFloat(value);
    }

    /**
     * @example
     * Check.equal(value, undefined)
     */
    export function isUndefined(value: unknown): value is undefined {
        return Check.equal(value, undefined);
    }
    /**
     * @example
     * !Check.isUndefined(value)
     */
    export function isNotUndefined<Value>(value: Value): value is Exclude<Value, undefined> {
        return !Check.isUndefined(value);
    }

    /**
     * @example
     * Check.equal(value, null)
     */
    export function isNull(value: unknown): value is null {
        return Check.equal(value, null);
    }
    /**
     * @example
     * !Check.isNull(value)
     */
    export function isNotNull<Value>(value: Value): value is Exclude<Value, null> {
        return !Check.isNull(value);
    }

    /**
     * @example
     * Check.isUndefined(value) || Check.isNull(value)
     */
    export function isUndefinedOrNull(value: unknown): value is undefined | null {
        return Check.isUndefined(value) || Check.isNull(value);
    }
    /**
     * @example
     * Check.isNotUndefined(value) && Check.isNotNull(value)
     */
    export function isNotUndefinedAndNotNull<Value>(value: Value): value is Exclude<Value, undefined | null> {
        return Check.isNotUndefined(value) && Check.isNotNull(value);
    }

    /**
     * @example
     * Check.equal(typeof value, 'symbol')
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isSymbol(value: unknown): value is symbol {
        return Check.equal(typeof value, 'symbol');
    }
    /**
     * @example
     * !Check.isSymbol(value)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isNotSymbol<Value>(value: Value): value is Exclude<Value, symbol> {
        return !Check.isSymbol(value);
    }

    /**
     * @example
     * Check.equal(typeof value, 'function')
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isFunction(value: unknown): value is FunctionType {
        return Check.equal(typeof value, 'function');
    }
    /**
     * @example
     * !Check.isFunction(value)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isNotFunction<Value>(value: Value): value is Exclude<Value, FunctionType> {
        return !Check.isFunction(value);
    }

    /**
     * @example
     * Check.equal(typeof value, 'object') && Check.isNotNull(value)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isObject(value: unknown): value is object {
        return Check.equal(typeof value, 'object') && Check.isNotNull(value);
    }
    /**
     * @example
     * !Check.isObject(value)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isNotObject<Value>(value: Value): value is Exclude<Value, object> {
        return !Check.isObject(value);
    }

    /**
     * @example
     * Array.isArray(value)
     */
    export function isArray(value: unknown): value is Array<unknown> | ReadonlyArray<unknown> {
        return Array.isArray(value);
    }
    /**
     * @example
     * !Check.isArray(value)
     */
    export function isNotArray<Value>(value: Value): value is Exclude<Value, Array<unknown> | ReadonlyArray<unknown>> {
        return !Check.isArray(value);
    }

    /**
     * @example
     * Check.isFunction(value) && Check.isObject(value.prototype) && Check.equal(value.prototype.constructor, value)
     */
    export function isConstructor(value: unknown): value is ConstructorType {
        return Check.isFunction(value) && Check.isObject(value.prototype) && Check.equal(value.prototype.constructor, value);
    }
    /**
     * @example
     * !Check.isConstructor(value)
     */
    export function isNotConstructor<Value>(value: Value): value is Exclude<Value, ConstructorType> {
        return !Check.isConstructor(value);
    }

    /**
     * @example
     * Check.isIn(Symbol.iterator, value)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Symbol/iterator
     */
    export function isIterable(value: object): value is Iterable<unknown> {
        return Check.isIn(Symbol.iterator, value);
    }
    /**
     * @example
     * !Check.isIterable(value)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Symbol/iterator
     */
    export function isNotIterable<Value extends object>(value: Value): value is Exclude<Value, Iterable<unknown>> {
        return !Check.isIterable(value);
    }

    /**
     * @example
     * Check.isIn(Symbol.asyncIterator, value)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Symbol/asyncIterator
     */
    export function isAsyncIterable(value: object): value is AsyncIterable<unknown> {
        return Check.isIn(Symbol.asyncIterator, value);
    }
    /**
     * @example
     * !Check.isAsyncIterable(value)
     *
     * @see
     * https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Symbol/asyncIterator
     */
    export function isNotAsyncIterable<Value extends object>(value: Value): value is Exclude<Value, AsyncIterable<unknown>> {
        return !Check.isAsyncIterable(value);
    }
}
