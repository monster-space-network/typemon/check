# [5.2.0](https://gitlab.com/monster-space-network/typemon/check/compare/5.1.0...5.2.0) (2020-08-17)


### Features

* is-not-integer, is-not-float 추가 ([a0009c4](https://gitlab.com/monster-space-network/typemon/check/commit/a0009c46a1fb6b46029ee254086433b4e56c9f78))



# [5.1.0](https://gitlab.com/monster-space-network/typemon/check/compare/5.0.0...5.1.0) (2020-07-17)


### Features

* is-async-iterable, is-not-async-iterable 추가 ([4d745ac](https://gitlab.com/monster-space-network/typemon/check/commit/4d745acb4e34f59ac7e8b9361c7bffbce160211c))
* **instance-of:** 유형 추론 개선 ([dea664e](https://gitlab.com/monster-space-network/typemon/check/commit/dea664e204c172837c65978c31f641c6109fe5dc))
* **is-constructor:** 유형 추론 개선 ([cb9161e](https://gitlab.com/monster-space-network/typemon/check/commit/cb9161eb37d36607368f64cdbdc335a0a8ea5815))
* **is-function:** 유형 추론 개선 ([2983097](https://gitlab.com/monster-space-network/typemon/check/commit/29830975a5c27bf7ca18a9d00c76696007063295))
* **is-iterable:** 링크 변경 ([4480749](https://gitlab.com/monster-space-network/typemon/check/commit/44807490d0ae4bc15b1f7c3c2cc8abe065afa832))
* 함수, 생성자 유형 추가 ([4993d31](https://gitlab.com/monster-space-network/typemon/check/commit/4993d315f66bab070caa9a9c90e6cfefa6563e69))



# [5.0.0](https://gitlab.com/monster-space-network/typemon/check/compare/4.0.0...5.0.0) (2020-07-16)


### Features

* **is-in:** 유형 가드 기능 추가 ([402551a](https://gitlab.com/monster-space-network/typemon/check/commit/402551a79e4a804ac8b2d48ef47ac766ffff858b))
* **is-iterable:** 재작성 ([42cfb09](https://gitlab.com/monster-space-network/typemon/check/commit/42cfb0924daa5504445afb8ebf8fd1a376a2c8ba))
* is-not-integer, is-not-float 삭제 ([fef3bc0](https://gitlab.com/monster-space-network/typemon/check/commit/fef3bc0cc33101eb6ed7725fee9fabdd6eaf23aa))



# [4.0.0](https://gitlab.com/monster-space-network/typemon/check/compare/3.0.0...4.0.0) (2020-01-12)


### Bug Fixes

* **is-iterable:** 잘못된 주석 수정 ([5335013](https://gitlab.com/monster-space-network/typemon/check/commit/53350132eb2dca446f97eccfa19a160c2309f02f))
* **is-string, is-number:** 누락된 typeof 연산자 추가 ([858cf80](https://gitlab.com/monster-space-network/typemon/check/commit/858cf80e90f07ca32b222aebacb6fe672e7e7cfd))


### Features

* is 삭제 ([f1c6a05](https://gitlab.com/monster-space-network/typemon/check/commit/f1c6a051653af364d0ba9c24976fe0555101cd30))
* is-empty-string 삭제 ([ae479a1](https://gitlab.com/monster-space-network/typemon/check/commit/ae479a16e6dd29d43a87d854e32e3f6f5ed4cfbe))
* is-empty-string 추가 ([dd67e17](https://gitlab.com/monster-space-network/typemon/check/commit/dd67e171d2b2113432fbe00a35238930c72c7b8f))
* is-in 추가 ([6bf6f00](https://gitlab.com/monster-space-network/typemon/check/commit/6bf6f00fc7b13875eb13327d8a4272444f47022f))
* is-instance 삭제 ([42092df](https://gitlab.com/monster-space-network/typemon/check/commit/42092dfdb6f0aaebce7b4cda29ac0df9b292af7e))
* 의존성 업데이트 ([283a9e4](https://gitlab.com/monster-space-network/typemon/check/commit/283a9e42268567da741bfc4c0f50fb09b5ba36cb))



# [3.0.0](https://gitlab.com/monster-space-network/typemon/check/compare/2.0.0...3.0.0) (2019-10-17)


### Features

* **instance-of:** 주석 추가 ([882db96](https://gitlab.com/monster-space-network/typemon/check/commit/882db9641aa06a31d725cca982044191b36442c6))
* equal 추가 ([f874cdb](https://gitlab.com/monster-space-network/typemon/check/commit/f874cdbaec125b2c39e4773da54c69b68bd3b267))
* greater, greater-or-equal, less, less-or-equal 추가 ([5659885](https://gitlab.com/monster-space-network/typemon/check/commit/5659885deab7c2a4cf9dc0c516d9c85ebeb84a24))
* instance-of 추가 ([da5f14d](https://gitlab.com/monster-space-network/typemon/check/commit/da5f14d1cb909ffdb290e584b00119db2f6bbf51))
* is 추가, 주석 추가 ([e420498](https://gitlab.com/monster-space-network/typemon/check/commit/e420498568f96bd6e31bf1a9c682210d86aecbcc))
* is-empty-string 삭제 ([8a1d4ff](https://gitlab.com/monster-space-network/typemon/check/commit/8a1d4ffadd9d31b5d51bb9d088b2fbd8ba415e60))
* is-safe-integer 삭제 ([507ca14](https://gitlab.com/monster-space-network/typemon/check/commit/507ca14d4118abbfc7293cc870cc37cfbb6469cd))
* 의존성 업데이트 ([1aee8f1](https://gitlab.com/monster-space-network/typemon/check/commit/1aee8f13de54f9a1eb737d613924460900ba8f68))
* 인스턴스 확인 기능 삭제 ([d34d420](https://gitlab.com/monster-space-network/typemon/check/commit/d34d42045c9e17ce514bdbf94b3da0dd4781dbff))
* 주석 추가 ([9c525bc](https://gitlab.com/monster-space-network/typemon/check/commit/9c525bc7175c00cd0f1ca64250f765960e908659))



# [2.0.0](https://gitlab.com/monster-space-network/typemon/check/compare/1.5.0...2.0.0) (2019-09-18)


### Features

* is-empty, is-empty-array, is-json 삭제 ([f333463](https://gitlab.com/monster-space-network/typemon/check/commit/f333463))
* is-iterable 추가 ([c6cc08d](https://gitlab.com/monster-space-network/typemon/check/commit/c6cc08d))
* is-positive-number, is-negative-number 삭제 ([d0b4148](https://gitlab.com/monster-space-network/typemon/check/commit/d0b4148))
* is-zero 삭제 ([9de2ecc](https://gitlab.com/monster-space-network/typemon/check/commit/9de2ecc))
* 일부 기능 개선 및 주석 수정 ([3fb2fc4](https://gitlab.com/monster-space-network/typemon/check/commit/3fb2fc4))



# [1.5.0](https://gitlab.com/monster-space-network/typemon/check/compare/1.4.0...1.5.0) (2019-07-17)


### Features

* **package:** 개발 종속성 업데이트 ([8de654f](https://gitlab.com/monster-space-network/typemon/check/commit/8de654f))
* is-empty-array, is-not-empty-array 추가 ([362c9ce](https://gitlab.com/monster-space-network/typemon/check/commit/362c9ce))



# [1.4.0](https://gitlab.com/monster-space-network/typemon/check/compare/1.3.0...1.4.0) (2019-06-05)


### Bug Fixes

* **is-float, is-not-float:** 로직 수정 ([0ebfe6c](https://gitlab.com/monster-space-network/typemon/check/commit/0ebfe6c))


### Features

* is-error, is-not-error 추가 ([e6e4abc](https://gitlab.com/monster-space-network/typemon/check/commit/e6e4abc))
* is-not-true, is-not-false 추가 ([d26d607](https://gitlab.com/monster-space-network/typemon/check/commit/d26d607))



# [1.3.0](https://gitlab.com/monster-space-network/typemon/check/compare/1.2.0...1.3.0) (2019-05-13)


### Features

* **is-constructor, is-instance:** 로직 변경 ([37c0248](https://gitlab.com/monster-space-network/typemon/check/commit/37c0248))



# [1.2.0](https://gitlab.com/monster-space-network/typemon/check/compare/1.1.0...1.2.0) (2019-05-11)


### Features

* 비교 기능 삭제 ([c23b7eb](https://gitlab.com/monster-space-network/typemon/check/commit/c23b7eb))
* **is-object, is-not-object:** null 확인 로직 추가 ([407e73c](https://gitlab.com/monster-space-network/typemon/check/commit/407e73c))
* 클래스 생성자, 인스턴스 유형 확인 기능 추가 ([8fc908e](https://gitlab.com/monster-space-network/typemon/check/commit/8fc908e))




# [1.1.0](https://gitlab.com/monster-space-network/typemon/check/compare/1.0.0...1.1.0) (2019-05-07)


### Bug Fixes

* **is-array:** 제네릭 유형 수정 ([6b09661](https://gitlab.com/monster-space-network/typemon/check/commit/6b09661))


### Features

* Set, Map 유형 확인 기능 추가 ([91cfb16](https://gitlab.com/monster-space-network/typemon/check/commit/91cfb16))
