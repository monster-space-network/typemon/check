# Check - [![version](https://img.shields.io/npm/v/@typemon/check.svg)](https://www.npmjs.com/package/@typemon/check) [![license](https://img.shields.io/npm/l/@typemon/check.svg)](https://gitlab.com/monster-space-network/typemon/check/blob/master/LICENSE) ![typescript-version](https://img.shields.io/npm/dependency-version/@typemon/check/dev/typescript.svg) [![gitlab-pipeline](https://gitlab.com/monster-space-network/typemon/check/badges/master/pipeline.svg)](https://gitlab.com/monster-space-network/typemon/check/-/pipelines) [![coverage](https://gitlab.com/monster-space-network/typemon/check/badges/master/coverage.svg)](https://gitlab.com/monster-space-network/typemon/check/-/graphs/master/charts)
A collection of frequently used, safe and intuitive type checking and comparison functions.
Resolve inconsistencies between global functions and methods of primitive wrapper objects.
You no longer need to write duplicate code or use exclamation marks.
It provides the most basic functionality without dependencies, so you don't have to worry about overhead or code size.
Fully integrated with the TypeScript type system, you can expect high productivity.



## Features
Each function is well documented through comments.
It is also easy to understand because it is linked to MDN documents.
- `equal, notEqual`
- `less, lessOrEqual`
- `greater, greaterOrEqual`
- `isIn, isNotIn`
- `instanceOf, notInstanceOf`
- `isBoolean, isNotBoolean`
- `isTrue, isNotTrue`
- `isFalse, isNotFalse`
- `isTruthy, isFalsy`
- `isString, isNotString`
- `isNumber, isNotNumber`
- `isNaN, isNotNaN`
- `isFinite, isInfinite`
- `isInteger, isNotInteger`
- `isFloat, isNotFloat`
- `isUndefined, isNotUndefined`
- `isNull, isNotNull`
- `isUndefinedOrNull, isNotUndefinedAndNotNull`
- `isSymbol, isNotSymbol`
- `isFunction, isNotFunction`
- `isObject, isNotObject`
- `isArray, isNotArray`
- `isConstructor, isNotConstrutor`
- `isIterable, isNotIterable`
- `isAsyncIterable, isNotAsyncIterable`



## Installation
```
$ npm install @typemon/check
```



## Usage
```typescript
import { Check } from '@typemon/check';
```
```typescript
const value: boolean | number | string | . . . = generateValue();

if (Check.isBoolean(value)) {
    . . .
}
else if (Check.isNumber(value)) {
    . . .
}
else {
    . . .
}
```
```typescript
const value: Example | typeof Example = generateValue();

if (Check.isConstructor(value)) {
    . . .
}
else {
    . . .
}
```
